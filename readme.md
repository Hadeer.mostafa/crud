[![Build Status](https://travis-ci.org/RutledgePaulV/CRUD.svg?branch=develop)](https://travis-ci.org/RutledgePaulV/CRUD)
[![Coverage Status](https://coveralls.io/repos/github/RutledgePaulV/CRUD/badge.svg?branch=develop)](https://coveralls.io/github/RutledgePaulV/CRUD?branch=develop)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.github.rutledgepaulv/CRUD/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.github.rutledgepaulv/CRUD)





Release Versions
```xml
<dependencies>
    <dependency>
        <groupId>CRUD</groupId>
        <artifactId>CRUD</artifactId>
        <version><!-- Not yet released --></version>
    </dependency>
</dependencies>
```

Snapshot Versions
```xml
<dependencies>
    <dependency>
        <groupId>CRUD</groupId>
        <artifactId>CRUD</artifactId>
        <version>0.0.1-SNAPSHOT</version>
    </dependency>
</dependencies>

<repositories>
    <repository>
        <id>ossrh</id>
        <name>Repository for snapshots</name>
        <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        <snapshots>
            <enabled>true</enabled>
        </snapshots>
    </repository>
</repositories>
```


This project is licensed under [MIT license](http://opensource.org/licenses/MIT).
