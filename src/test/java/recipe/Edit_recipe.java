package recipe;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import Core.Base;

public class Edit_recipe extends Base {

	@Test(priority = 1)
	public void Edit() throws InterruptedException {



		WebElement click = driver.findElement(By.xpath("/html/body/div/div[1]/div/div/div[4]/div[1]/h4/a"));
		click.click();

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,600)"); 

		WebElement edit = driver.findElement(By.id("btn-edit3"));
		edit.click();

		WebElement recipe_name = driver.findElement(By.id("title"));
		recipe_name.clear();
		recipe_name.sendKeys("Recipe Name is Edited");

		WebElement ingredients = driver.findElement(By.id("ingredients"));
		ingredients.clear();
		ingredients.sendKeys("test 4 , test 5 , test 6");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		WebElement save = driver.findElement(By.id("addButton"));
		save.click();





	}


	@Test(priority = 2) public void Assertion() throws InterruptedException {

		WebElement open_recipe = driver.findElement(By.xpath("/html/body/div/div[1]/div/div/div[5]/div[1]/h4/a"));
		open_recipe.click();

		WebElement edit_recipe =driver.findElement(By.xpath("/html/body/div/div[1]/div/div/div[5]"));
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(edit_recipe));
		Assert.assertEquals("Recipe Name is Edited", open_recipe.getText());
		System.out.println(open_recipe.getText() + " is displayed – Assert passed");







	}













}