package recipe;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import Core.Base;

public class Add_recipe extends Base {

	@Test(priority = 1)
		public void Resize() throws InterruptedException {
		
		// Resize the upper frame

		WebElement resizeContainer = driver.findElement(By.id("resizer"));
		Actions actionsResize = new Actions(driver);
		actionsResize.dragAndDropBy(resizeContainer, 0,-300).perform();

		// Switching to the other frame to locate elements
		
		driver.switchTo().frame("result");

		}
	@Test(priority = 2)	
	public void Add() throws InterruptedException {

		WebElement Add_button = driver.findElement(By.id("show"));
		Add_button.click();


		WebElement recipe_name = driver.findElement(By.id("title"));
		recipe_name.sendKeys("Recipe Name");

		WebElement ingredients = driver.findElement(By.id("ingredients"));
		ingredients.sendKeys("test 1 , test 2 , test 3");
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		WebElement save = driver.findElement(By.id("addButton"));
		save.click();
	}

	@Test(priority = 3)
	public void Assertion() throws InterruptedException {

		// Assert if the new Recipe is added successfully

		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		WebElement new_recipe = driver.findElement(By.xpath("/html/body/div/div[1]/div/div/div[4]/div[1]/h4"));
		WebDriverWait wait = new WebDriverWait(driver,10);
		wait.until(ExpectedConditions.visibilityOf(new_recipe));
		Assert.assertEquals("Recipe Name", new_recipe.getText());
		System.out.println(new_recipe.getText() + " is displayed – Assert passed");


		
		


		
		

	}

















}
