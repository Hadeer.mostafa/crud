package recipe;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import Core.Base;

public class Delete_recipe extends Base {
	
	
	@Test(priority = 1)
	public void Delete() throws InterruptedException {

	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	WebElement open_recipe = driver.findElement(By.xpath("/html/body/div/div[1]/div/div/div[5]/div[1]/h4/a"));
	open_recipe.click();
	
	WebElement Delete = driver.findElement(By.id("btn-del4"));
	WebDriverWait wait = new WebDriverWait(driver,10);
	wait.until(ExpectedConditions.visibilityOf(Delete));
	Delete.click();
	
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	WebElement All_recipes = driver.findElement(By.id("container"));
	Assert.assertEquals( All_recipes.getText(),"Pumpkin Pie " + "Spaghetti "+ "Onion Pie " );



	

	
	}
}
